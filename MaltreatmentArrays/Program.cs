﻿using System;

namespace MaltreatmentArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Random losowe = new Random();

            int[] tablica = new int[100];
            int suma = 0, dominanta = -1, licznik = 0, rekord = 0, tmp;

            for (int i = 0; i < tablica.Length; i++)
            {
                tablica[i] = losowe.Next(1001);
                suma += tablica[i];
            }

            for (int j = 0; j <= tablica.Length; j++)
            {
                for (int i = 0; i < tablica.Length - 1; i++)
                {
                    if (tablica[i] > tablica[i + 1])
                    {
                        tmp = tablica[i];
                        tablica[i] = tablica[i + 1];
                        tablica[i + 1] = tmp;
                    }
                }
            }

            foreach (int element in tablica)
                Console.Write(element + ", ");

            for (int j = 0; j <= 1000; j++)
            {
                for (int i = 0; i < tablica.Length; i++)
                {
                    if (tablica[i] == j) licznik++;
                }

                if (licznik > 1 && licznik > rekord)
                {
                    rekord = licznik;
                    dominanta = j;
                }

                licznik = 0;
            }

            Console.WriteLine("\n\nLiczba elementów wynosi: {0}", tablica.Length);
            Console.WriteLine("Suma elementów wynosi: {0}", suma);
            Console.WriteLine("Średnia elementów wynosi: {0}", ((double)suma) / ((double)tablica.Length));

            if (dominanta != -1)
                Console.WriteLine("Dominanta wynosi: {0}", dominanta);

            if (tablica.Length % 2 == 1)
                Console.WriteLine("Mediana wynosi: {0}", tablica[(tablica.Length - 1) / 2]);
            else
            {
                double mediana = ((double)tablica[tablica.Length / 2 - 1] + (double)tablica[tablica.Length / 2]) / 2;
                Console.WriteLine("Mediana wynosi: {0}", mediana);
            }

        }
    }
}
